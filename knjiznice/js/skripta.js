/* global L, distance, global $ */


var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
  

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 50
    
    // maxZoom: 3
  };


  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);
  

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Ročno dodamo fakulteto za računalništvo in informatiko na mapo
  dodajMarker(FRI_LAT, FRI_LNG, "FAKULTETA ZA RAČUNALNIŠTVO IN INFORMATIKO", "FRI");

  // Objekt oblačka markerja
  var popup = L.popup();
                                                                                                  
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);
      dodajMarker(latlng.lat, latlng.lng);
      for(var i = 0; i < koord.length; i++){
        var poly = L.polygon(koord[i], {color: "#008000", weight: 1}).addTo(mapa);
      }
  }
  
  var y = [];
    var pacNekaj = 0;
    var koord = [];
    //jQuery(document).ready(function() {
        // all custom jQuery will go here
        jQuery.getJSON("knjiznice/json/bolnisnice.json", function(data){
          
         
          for(var i = 0; i < data.features.length - 10; i++){
            //y[0] = data.features[i].geometry.coordinates[1];
            
            if(data.features[i].geometry.coordinates[0].length > 1){
              y = data.features[i].geometry.coordinates[0];
              //console.log(y);
            }
            if(y.length > 2){
              for(var j = 0; j < y.length; j++){
                [y[j][0], y[j][1]] = [y[j][1], y[j][0]]
              }
              koord[i] = y;
              //var poly = L.polygon(y, {color: "#008000", weight: 1}).addTo(mapa);
            }
            //var polygon = L.polygon(y);
            //mapa.addLayer(polygon);
            //console.log(y);
            
          }
          //console.log(y);
          //mapa.addLayer(polygon);
        });
    //});
  
  mapa.on('click', obKlikuNaMapo);
  
    
    
});


/**
 * Na zemljevid dodaj oznake z bližnjimi fakultetami in
 * gumb onemogoči.
 */
function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      (tip == 'FRI' ? 'red' : (tip == 'restaurant' ? 'green' : 'blue')) + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}



/*

var x = [];
var y = []
jQuery(document).ready(function() {
    // all custom jQuery will go here
    jQuery.getJSON("knjiznice/json/bolnisnice.json", function(data){
      
      for(var i = 0; i < data.length; i++){
        console.log(data[i].features); 
      }
    });
});
*/
